<?php declare(strict_types=1);


namespace App\Controller;


use App\Form\NewFeedType;
use App\Message\NewFeed;
use FeedIo\Factory;
use FeedIo\FeedInterface;
use FeedIo\FeedIo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class DefaultController extends AbstractController
{

    private FeedIo $feedIo;

    private \Redis $redis;

    private MessageBusInterface $bus;

    public function __construct(\Redis $redis, MessageBusInterface $bus)
    {
        $this->feedIo = Factory::create()->getFeedIo();
        $this->redis = $redis;
        $this->bus = $bus;
    }

    public function index(Request $request): Response
    {
        $form = $this->createForm(NewFeedType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($this->canProcess($data)) {
                $newFeed = new NewFeed($data['url']);
                $this->bus->dispatch($newFeed);
                $this->addFlash('success', 'feed successfully submitted');
            }
         }

        return $this->render('submit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function canProcess(array $data): bool
    {
        $url = filter_var($data['url'], FILTER_VALIDATE_URL);
        if ( ! $url ) {
            $this->addFlash('error', 'invalid URL');
            return false;
        }

        if ($this->redis->get('url_' . $url)) {
            $this->addFlash('error', 'feed already submitted');
            return false;
        }

        $this->redis->set('url_' . $url, time());

        try {
            $feed = $this->feedIo->read($url)->getFeed();
            if ( ! $feed instanceof FeedInterface ) {
                throw new \RuntimeException();
            }
        } catch (\Exception $e) {
            $this->addFlash('error', 'not a valid feed');
            return false;
        }

        return true;
    }

}

build:
	docker-compose build --build-arg DOCKER_UID=$(shell id -u)
	docker-compose run submitter composer install

start:
	docker-compose up -d

stop:
	docker-compose down

shell:
	docker-compose run fpm bash

build-image:
	git clone git@github.com:alexdebril/feed-io-webapp-submitter.git /tmp/feed-io-webapp-submitter
	cd /tmp/feed-io-webapp-submitter && docker build --no-cache . -t alexdebril/feed-io-webapp-submitter
	rm -Rf /tmp/feed-io-webapp-submitter

publish-image:
	$(MAKE) build-image
	docker push alexdebril/feed-io-webapp-submitter